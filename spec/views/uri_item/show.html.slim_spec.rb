require 'spec_helper'

describe "uri_item/show.html.slim" do
  fixtures :users, :services, :uri_items, :comments

  subject(:item1){
    UriItem.where(url:"http://alfalfalfa.com/archives/1897365.html").first()
  }
  context :with_exist_item_id do
    before do
      assign(:item, item1)
      render
    end
    it "render _item partial for the item" do
      expect(view).to render_template(:partial => "_item", :count => 1)
      expect(view).to render_template(:partial => "_comment", :count => item1.comments.size)
    end
  end
end
