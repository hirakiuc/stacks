require 'spec_helper'

describe "timeline/index.html.slim" do
  include LoginHelper
  fixtures :users, :services, :uri_items, :comments

  subject(:user1){ User.where(email:'test1@gmail.com').first() }

  context :without_signin do
    before do
      signout()
      assign(:items, [stub_model(UriItem), stub_model(UriItem)])
      render
    end
    it "renders _item partial for each item" do
      expect(view).to render_template(:partial => "_post_form", :count => 1)
      expect(view).to render_template(:partial => "_item", :count => 2)
    end
  end

  context :with_signin do
    before do
      signin(user1)
      assign(:items, [stub_model(UriItem), stub_model(UriItem)])
      render
    end
    it "renders _item partial for each item" do
      expect(view).to render_template(:partial => "_post_form", :count => 1)
      expect(view).to render_template(:partial => "_item", :count => 2)
    end
  end
end
