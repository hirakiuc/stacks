module LoginHelper
  def signin(user)
    @request.env["device.mapping"] = Devise.mappings[:user]
    @controller.stub(:user_signed_in?).and_return true
    @controller.stub(:current_user).and_return user
    @controller.stub(:authenticate_user!).and_return true
  end

  def signout
    @request.env["device.mapping"] = Devise.mappings[:user]
    @controller.stub(:user_signed_in?).and_return false
    @controller.stub(:current_user).and_return nil
  end

  def signin?
    @controller.send(:user_signed_in?)
  end
end
