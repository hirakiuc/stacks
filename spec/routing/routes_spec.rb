require 'spec_helper'

describe "route to root_url" do
  it "routes / to top#index" do
    expect(:get => '/').to route_to(
      :controller => 'top',
      :action => 'index'
    )
  end
end

describe "route to top/index" do
  it "to top#index" do
    expect(:get => '/top/index').to route_to(
      controller: 'top',
      action: 'index'
    )
  end
end

describe "route to timeline_index_path" do
  it "routes /timeline/index to timeline#index" do
    expect(:get => '/timeline/index').to route_to(
      controller: 'timeline',
      action: 'index'
    )
  end
end

describe "route to services_index_path" do
  it "routes /services/index to services#index" do
    expect(:get => '/services/index').to route_to(
      :controller => 'services',
      :action => 'index'
    )
  end
end

describe "route to user_omniauth_authorize_path" do
  it "to service/callback#passthru {:provider => ...}" do

    %w|facebook twitter gplus|.each do |provider|
      expect(:get  => "/user/auth/#{provider}").to route_to(
        :controller => 'service/callback',
        :action => "passthru",
        :provider => provider
      )
      expect(:post => "/user/auth/#{provider}").to route_to(
        :controller => 'service/callback',
        :action => "passthru",
        :provider => provider
      )
    end
  end
end

describe "route to user_omniauth_callback" do

  it "to service/callback#:provider" do
    %w|facebook twitter gplus|.each do |provider|
      expect(:get => "/user/auth/#{provider}/callback").to route_to(
        :controller => "service/callback",
        :action => provider
      )
      expect(:post => "/user/auth/#{provider}/callback").to route_to(
        :controller => "service/callback",
        :action => provider
      )
    end
  end
end

describe "route to uri_item#show" do
  context :with_id do
    it "to uri_item#show" do
      expect(:get => "/item/10").to route_to(
        controller: 'uri_item',
        action: 'show',
        id: 10.to_s
      )
    end
  end
end

describe "route to /api/thumbs_up" do
  context "with post method" do
    it "to /api/thumbs_up#create" do
      expect(:post => '/api/thumbs_up').to route_to(
        controller: 'api/thumbs_up',
        action: 'create',
        format: 'json'
      )
    end
  end

  context "with delete method" do
    it "to /api/thumbs_up#destroy" do
      expect(:delete => '/api/thumbs_up/10').to route_to(
        controller: 'api/thumbs_up',
        action: 'destroy',
        id: 10.to_s,
        format: 'json'
      )
    end
  end
end

describe "route to /api/item" do
  context "with post method" do
    it "to /api/item#create" do
      expect(:post => '/api/item').to route_to(
        controller: 'api/item',
        action: 'create',
        format: 'json'
      )
    end
  end
end
