require 'spec_helper'

describe Service do

  describe :attr_accessible do
    attrs = %w|provider email uid name user_id|
    attrs.each do|attr|
      it { should allow_mass_assignment_of(attr.intern) }
    end

    (Service.new.attributes.keys - attrs).each do |attr|
      it { should_not allow_mass_assignment_of(attr.intern) }
    end
  end

  describe :validate_definition do
    subject{ Service.new }
    it { expect(subject).to validate_presence_of(:provider) }
    it { expect(subject).to validate_presence_of(:name) }
    it { expect(subject).to validate_presence_of(:uid) }
    it { expect(subject).to validate_uniqueness_of(:uid).scoped_to(:provider) }
  end

  describe :relation do
    it { should belong_to(:user) }
  end
end
