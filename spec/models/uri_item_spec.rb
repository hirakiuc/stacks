require 'spec_helper'

describe UriItem do

  describe :validation do
    subject{ UriItem.new() }
    it { expect(subject).to validate_presence_of(:url) }
    it { expect(subject).to validate_uniqueness_of(:url) }
  end

  describe :relation do
    subject{ UriItem.new }
    it { expect(subject).to have_many(:comments) }
    it { expect(subject).to have_and_belong_to_many(:users).order("last_sign_in_at DESC") }
  end

  describe :attr_accessible do
    attrs = %w|url title favicon_url|
    attrs.each do |attr|
      it { should allow_mass_assignment_of(attr.intern) }
    end

    (UriItem.new.attributes.keys - attrs).each do |attr|
      it { should_not allow_mass_assignment_of(attr.intern) }
    end
  end
end
