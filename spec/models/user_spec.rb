require 'spec_helper'

describe User do
  fixtures :users, :uri_items, :comments

  describe :relation do
    subject{ User.new }
    it { expect(subject).to have_many(:services) }

    # TODO can't test limit
    it { expect(subject).to have_and_belong_to_many(:uri_items).order("created_at DESC") }
    # TODO can't test limit
    it { expect(subject).to have_many(:comments).order("created_at DESC") }
  end

  describe :validation do
    subject{ User.new }
    it { expect(subject).to validate_presence_of(:name) }
    it { expect(subject).to ensure_length_of(:name).is_at_least(4).is_at_most(30) }
    it { expect(subject).to validate_uniqueness_of(:name) }
    it { expect(subject).to validate_uniqueness_of(:email) }
  end

  describe :relation do
    subject{ User.new }
    it { expect(subject).to have_many(:services) }
    it { expect(subject).to have_and_belong_to_many(:uri_items) }
    it { expect(subject).to have_many(:comments) }
  end

end
