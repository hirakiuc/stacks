require 'spec_helper'

describe Comment do
  describe :attr_accessible do
    attrs = %w|text|
    attrs.each do|attr|
      it { should allow_mass_assignment_of(attr.intern) }
    end

    (Comment.new.attributes.keys - attrs).each do |attr|
      it { should_not allow_mass_assignment_of(attr.intern) }
    end
  end

  describe :validation do
    it { should validate_presence_of(:text) }
  end

  describe :relation do
    it { should belong_to(:uri_item) }
    it { should belong_to(:user) }
  end

  describe "scope" do
    fixtures :users, :uri_items, :comments
    subject(:user1){ User.where(:email => 'test1@gmail.com').first() }
    describe :of_user do
      it "finds the user's comments" do
        expect(subject).not_to be_nil
        comments = Comment.of_user(subject).order("created_at DESC").all()

        expect(comments).not_to be_nil
        expect(comments.size).to be(2)
        comments.each do |comment|
          expect(comment.user.email).to eq('test1@gmail.com')
        end
      end
    end
  end
end
