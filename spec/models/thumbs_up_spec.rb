require 'spec_helper'

describe ThumbsUp do
  fixtures :users, :uri_items, :thumbs_ups

  subject(:user1){ User.find_by_email("test1@gmail.com") }
  subject(:google_url){ "https://www.google.co.jp" }

  describe :relation do
    subject{ ThumbsUp.new }
    it { expect(subject).to belong_to(:user) }
    it { expect(subject).to belong_to(:uri_item) }

    it { expect(subject).to validate_uniqueness_of(:uri_item_id).scoped_to(:user_id) }
  end

  describe :scope do
    describe :for_url do
      it "find with for_url" do
        expect(ThumbsUp.for_url(google_url).first()).not_to be_nil
      end
    end

    describe :for_user do
      it "find with for_user" do
        expect(ThumbsUp.for_user(user1).first()).not_to be_nil
      end
    end
  end

  describe "self.create_with_user_and_url" do
    pending "TODO add spec"
  end
end
