require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the Service::CallbackHelper. For example:
#
# describe Service::CallbackHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe Service::CallbackHelper do
  describe "parse_omniauth_for" do
    context :facebook do
      it "return empty string for each key" do
        h = helper.parse_omniauth_for(:facebook, {})
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:empty_auth){
        {
          'provider' => nil,
          'extra' => {
            'user_hash' => {}
          }
        }
      }
      it "return empty string for each key" do
        h = helper.parse_omniauth_for(:facebook, empty_auth)
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:auth){
        {
          'provider' => 'facebook',
          'extra' => {
            'user_hash' => {
              'email' => 'test@gmail.com',
              'name'  => 'test',
              'id'    => 2313123
            }
          }
        }
      }
      it "return value for each key" do
        h = helper.parse_omniauth_for(:facebook, auth)
        expect(h).to be_instance_of(Hash)

        user_hash = auth['extra']['user_hash']
        expect(h[:email]).to eq(user_hash['email'])
        expect(h[:name]).to eq(user_hash['name'])
        expect(h[:uid]).to eq(user_hash['id'].to_s())
        expect(h[:provider]).to eq(auth['provider'])
      end
    end

    context :github do
      it "return empty hash" do
        h = helper.parse_omniauth_for(:github, {})
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:empty_auth){
        {
          'provider' => nil,
          'user_info' => {},
          'extra' => {
            'user_hash' => {}
          }
        }
      }
      it "return empty string for each key" do
        h = helper.parse_omniauth_for(:github, empty_auth)
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:auth){
        {
          'provider' => 'github',
          'user_info' => {
            'email' => 'test@gmail.com',
            'name' => 'test'
          },
          'extra' => {
            'user_hash' => {
              'uid' => 20132
            }
          }
        }
      }
      it "return value for each key" do
        h = helper.parse_omniauth_for(:github, auth)
        expect(h).to be_instance_of(Hash)

        user_info = auth['user_info']
        expect(h[:email]).to eq(user_info['email'])
        expect(h[:name]).to eq(user_info['name'])

        user_hash = auth['extra']['user_hash']
        expect(h[:uid]).to eq(user_hash['id'].to_s())

        expect(h[:provider]).to eq(auth['provider'])
      end
    end

    context :twitter do
      it "return empty hash" do
        h = helper.parse_omniauth_for(:twitter, {})
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:empty_auth){
        {
          'provider' => nil,
          'uid' => nil,
          'info' => {}
        }
      }
      it "return empty string for each key" do
        h = helper.parse_omniauth_for(:twitter, empty_auth)
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:auth){
        {
          'provider' => 'twitter',
          'uid' => 23131,
          'info' => {
            'email' => 'test@gmail.com',
            'name' => 'test'
          }
        }
      }
      it "return value for each key" do
        h = helper.parse_omniauth_for(:twitter, auth)
        expect(h).to be_instance_of(Hash)

        user_info = auth['info']
        expect(h[:email]).to eq(user_info['email'])
        expect(h[:name]).to eq(user_info['name'])

        expect(h[:uid]).to eq(auth['uid'].to_s())

        expect(h[:provider]).to eq(auth['provider'])
      end
    end

    context :openid do
      it "return empty hash" do
        h = helper.parse_omniauth_for(:openid, {})
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:empty_auth){
        {
          'provider' => nil,
          'uid' => nil,
          'user_info' => {
            'email' => nil,
            'name' => nil
          }
        }
      }
      it "return empty string for each key" do
        h = helper.parse_omniauth_for(:openid, empty_auth)
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to be_empty
        expect(h[:name]).to be_empty
        expect(h[:uid]).to be_empty
        expect(h[:provider]).to be_empty
      end

      subject(:auth){
        {
          'provider' => 'openid_provider',
          'uid' => 232032,
          'user_info' => {
            'email' => 'test@gmail.com',
            'name'  => 'test'
          }
        }
      }
      it "return value for each key" do
        h = helper.parse_omniauth_for(:openid, auth)
        expect(h).to be_instance_of(Hash)

        expect(h[:email]).to eq(auth['user_info']['email'])
        expect(h[:name]).to  eq(auth['user_info']['name'])
        expect(h[:uid]).to   eq(auth['uid'].to_s)
        expect(h[:provider]).to eq(auth['provider'])
      end
    end

    context :unknown do
      it "return empty hash" do
        h = helper.parse_omniauth_for(:unknown, {})
        expect(h).to be_instance_of(Hash)

        expect(h.keys.size).to eq(0)
        expect(h.empty?).to be_true
      end
    end
  end
end
