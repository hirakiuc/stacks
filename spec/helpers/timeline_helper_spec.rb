require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the TimelineHelper. For example:
#
# describe TimelineHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe TimelineHelper do
  fixtures :users, :uri_items, :thumbs_ups

  def logged_in(user)
    @controller.stub(:user_signed_in?).and_return true
    @controller.stub(:current_user).and_return user
  end

  def not_logged_in
    @controller.stub(:user_signed_in?).and_return false
    @controller.stub(:current_user).and_return nil
  end

  subject(:item_google) { uri_items(:google) }
  subject(:item_craftone) { uri_items(:craftone) }
  subject(:user1) { users(:user1) }

  describe :thumbs_uped? do
    context :not_signed_in do
      before do
        not_logged_in()
      end

      it "return false" do
        expect(helper.thumbs_uped?(item_google)).to be_false
      end
    end

    context :signed_in do
      before do
        logged_in(user1)
      end

      it "return true with thumbs_uped item" do
        expect(helper.thumbs_uped?(item_google)).to be_true
      end

      it "return false with not thumbs_uped item" do
        expect(helper.thumbs_uped?(item_craftone)).to be_false
      end
    end
  end


  describe :thumbs_up_id do
    context :not_signed_in do
      before do
        not_logged_in()
      end

      it "return nil" do
        expect(helper.thumbs_up_id(item_google)).to be_nil
      end
    end

    context :signed_in do
      before do
        logged_in(user1)
      end

      it "return thumbs_up.id with thumbs_uped item" do
        expect(helper.thumbs_up_id(item_google)).not_to be_nil
      end

      it "return nil with not thumbs_uped item" do
        expect(helper.thumbs_up_id(item_craftone)).to be_nil
      end
    end
  end

end
