require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the TimelineHelper. For example:
#
# describe TimelineHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe ApiHelper do

  describe :valid_url? do
    def valid_url?(url)
      helper.send(:valid_url?, url)
    end

    it "return false with nil" do
      expect(valid_url?(nil)).to be_false
    end
    it "return false with non-string object" do
      expect(valid_url?({})).to be_false
    end
    it "return false with ftp-url" do
      expect(valid_url?("ftp://localhost/path/to/some")).to be_false
    end
    it "return true with http url" do
      expect(valid_url?("http://localhost/path/to/some")).to be_true
    end
    it "return true with https url" do
      expect(valid_url?("https://localhost/path/to/some")).to be_true
    end
  end

  describe :valid_token? do
    def valid_token?(token)
      helper.send(:valid_token?, token)
    end

    it "return false with nil" do
      valid_token?(nil)
    end
    it "return false with invalid text" do
      pending "implement validate token"
    end
    it "return true with some-text" do
      valid_token?('test')
    end
  end

  describe :respond_unauthorized_error do
    pending "TODO add spec"
  end

  describe :respond_parameter_error do
    pending "TODO add spec"
  end
end
