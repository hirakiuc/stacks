require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the Api::ItemHelper. For example:
#
# describe Api::ItemHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe Api::ItemHelper do
  describe :validate_item_request do
    def stub_url_params(url, token = nil)
      h = {}
      h[:url] = url if url
      h[:token] = token if token

      helper.stub(:params).and_return(h)
    end

    def stub_item_params(url, token = nil)
      h = {}
      h[:uri_item] = { :url => url } if url
      h[:token] = token if token

      helper.stub(:params).and_return(h)
    end

    def user_signed_in
      allow(helper).to receive(:user_signed_in?).and_return(true)
    end

    def user_not_signed_in
      allow(helper).to receive(:user_signed_in?).and_return(false)
    end

    def stub_responder_methods
      helper.stub(:respond_parameter_error){|url, msg| raise msg }
      helper.stub(:respond_unauthorized_error){|url, msg| raise msg }
    end

    def validate_item_request
      helper.validate_item_request()
    end

    subject(:valid_url){ "https://www.google.co.jp" }
    subject(:invalid_url){ "ftp://www.ruby-lang.org" }

    shared_examples_for "with url_patterns" do|signed_in|
      context "with valid url" do
        subject(:param_url){ valid_url }
        it "respond not signed in error" do
          if signed_in
            expect{ validate_item_request() }.not_to raise_error()
          else
            expect{ validate_item_request() }.to raise_error(/you are not signed in\./)
          end
        end
      end

      context "with invalid url" do
        subject(:param_url){ invalid_url }
        it "respond invalid url error" do
          expect{ validate_item_request() }.to raise_error(/invalid url/)
        end
      end

      context "without url" do
        subject(:param_url){ nil }
        it "respond invalid parameter error" do
          expect{ validate_item_request() }.to raise_error(/invalid parameter/)
        end
      end
    end

    context "not logged in" do
      context "with url param" do
        before do
          stub_url_params(param_url)
          stub_responder_methods()
          user_not_signed_in()
        end

        it_behaves_like "with url_patterns", false
      end

      context "with uri_item param" do
        before do
          stub_item_params(param_url)
          stub_responder_methods()
          user_not_signed_in()
        end

        it_behaves_like "with url_patterns", false
      end
    end

    context "logged in" do
      context "with url param" do
        before do
          stub_url_params(param_url)
          stub_responder_methods()
          user_signed_in()
        end

        it_behaves_like "with url_patterns", true
      end

      context "with uri_item param" do
        before do
          stub_item_params(param_url)
          stub_responder_methods()
          user_signed_in()
        end

        it_behaves_like "with url_patterns", true
      end
    end
  end
end
