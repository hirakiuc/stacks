require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the Api::ThumbsUpHelper. For example:
#
# describe Api::ThumbsUpHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe Api::ThumbsUpHelper do

  describe :validate_request do

    def stub_params(url, token = nil)
      h = {}
      h[:url] = url if url
      h[:token] = token if token

      helper.stub(:params) {h}
    end

    def user_signed_in
      allow(helper).to receive(:user_signed_in?).and_return(true)
    end

    def user_not_signed_in
      allow(helper).to receive(:user_signed_in?).and_return(false)
    end

    def stub_responder_methods
      helper.stub(:respond_parameter_error){|url, msg| raise msg }
      helper.stub(:respond_unauthorized_error){|url, msg| raise msg }
    end

    def validate_request
      helper.validate_request()
    end

    context "not logged in" do
      before do
        stub_params(param_url)
        stub_responder_methods()
        user_not_signed_in()
      end

      context "with valid url" do
        subject(:param_url){ "https://www.google.co.jp" }
        it "respond without error" do
          expect{ validate_request() }.to raise_error(/you are not signed in\./)
        end
      end

      context "with invalid url" do
        subject(:param_url){ "ftp://ruby-lang.org" }
        it "respond parameter error" do
          expect{ validate_request() }.to raise_error(/invalid url/)
        end
      end

      context "without url" do
        subject(:param_url){ nil }
        it "respond parameter error" do
          expect{ validate_request() }.to raise_error(/invalid parameter/)
        end
      end
    end

    context "logged in" do
      before do
        stub_params(param_url)
        stub_responder_methods()
        user_signed_in()
      end

      context "with valid url" do
        subject(:param_url){ "https://www.google.co.jp" }
        it "send action to controller" do
          expect{ validate_request() }.not_to raise_error
        end
      end

      context "with invalid url" do
        subject(:param_url){ "ftp://ruby-lang.org" }
        it "respond parameter error" do
          expect{ validate_request() }.to raise_error(/invalid url/)
        end
      end

      context "without url" do
        subject(:param_url){ nil }
        it "respond parameter error" do
          expect{ validate_request() }.to raise_error(/invalid parameter/)
        end
      end
    end

  end
end
