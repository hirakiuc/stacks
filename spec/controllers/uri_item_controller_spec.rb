require 'spec_helper'

describe UriItemController do
  fixtures :users, :uri_items

  describe "GET 'show'" do
    subject(:item_twitter_act_pattern){
      UriItem.where(url:"http://e0166.blog89.fc2.com/blog-entry-823.html").first()
    }
    before do
      get :show, id: item_twitter_act_pattern.id
    end
    it "returns http success" do
      expect(response.success?).to be_true
      expect(assigns[:item]).to eq(item_twitter_act_pattern)
    end
  end
end
