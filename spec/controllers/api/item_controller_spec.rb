require 'spec_helper'

describe Api::ItemController do
  fixtures :users, :uri_items

  subject(:user1){ User.find_by_email("test1@gmail.com") }
  subject(:invalid_url){ 'http://localhost/path/to/some' }
  subject(:valid_url){ UriItem.first().url }

  def logged_in(user)
    @controller.stub(:user_signed_in?).and_return true
    @controller.stub(:current_user).and_return user
  end

  def not_logged_in
    @controller.stub(:user_signed_in?).and_return false
    @controller.stub(:current_user).and_return nil
  end


  describe "POST 'create'" do
    context 'not logged in' do
      before do
        not_logged_in()
        request.accept = 'application/json'
        post :create, :format => 'json', :url => valid_url
      end

      it "returns http unauthorized" do
        expect(response).not_to be_success
        expect(response.status).to eq(401)
      end

      context :response_json do
        subject { JSON.parse(response.body) }

        it "['result']['status']" do
          expect(subject["result"]["status"]).to eq("failure")
        end
        it "['result']['msg']" do
          expect(subject["result"]["msg"]).to match(/^Unauthorized: /)
        end
        its(['url']) { should eq(valid_url) }
      end
    end

    context 'logged in' do
      before do
        logged_in(user1)
        request.accept = 'application/json'
        post :create, :format => 'json', :url => valid_url
      end

      it "returns http success" do
        expect(response).to be_success
      end

      context :response_json do
        subject { JSON.parse(response.body) }

        it "['result']['status']" do
          expect(subject["result"]["status"]).to eq("success")
        end
        it "['result']['msg']'" do
          expect(subject["result"]["msg"]).to be_nil
        end
        its(['url']) { should eq(valid_url) }
      end
    end
  end
end
