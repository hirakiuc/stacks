require 'spec_helper'

describe TopController do
  fixtures :users, :uri_items

  context "not logined" do
    describe "GET 'index'" do
      before do
        get :index
      end

      it "returns http success" do
        expect(response.success?).to be_true
      end
      it "renders the index template" do
        expect(response).to render_template('index')
      end
      it "not assign @items" do
        expect(assigns(:items)).to eq(
          UriItem.order(:created_at).limit(20).all()
        )
      end
    end
  end

  context "logined" do
    describe "GET 'index'" do
      subject(:user1){
        User.where(email: 'test1@gmail.com').first()
      }
      before do
        @controller.stub(:user_signed_in?).and_return true
        @controller.stub(:current_user).and_return user1
        get :index
      end

      it "returns http success" do
        expect(response.success?).to be_true
      end
      it "renders the index template" do
        expect(response).to render_template('index')
      end
      it "assign @items" do
        expect(assigns(:items)).to eq(user1.uri_items())
      end
    end
  end
end
