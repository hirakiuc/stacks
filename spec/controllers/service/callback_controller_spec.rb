require 'spec_helper'

describe Service::CallbackController do
  include LoginHelper
  fixtures :users, :services

  subject(:user1){ User.where(email: 'test1@gmail.com').first() }
  subject(:user1_twitter_service) {
    Service.find_by_provider_and_uid('twitter', 'user1_twitter_uid').first()
  }
  subject(:user2_facebook_service) {
    Service.find_by_provider_and_uid('facebook', 'user1_facebook_uid').first()
  }

  describe :user_authenticate_filter do
    context :without_signin do
      before do
        signout()
        get :facebook
      end
      it "redirect_to root_url" do
        expect(response.redirect?).to be_true
        expect(response).to redirect_to(root_url)
      end
    end
    context :with_signin do
      before do
        signin(user1)
        get :facebook
      end
      it "not redirected to root_url" do
        expect(response.redirect?).to be_true
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe :omniauth_filter do
    context :without_omniauth_info do
      before do
        signin(user1)
        get :facebook
      end
      it "redirect to new_user_session_path" do
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:error]).not_to be_nil
      end
    end

    context :with_empty_omniauth do
      before do
        signin(user1)
        request.env['omniauth.auth'] = {}
        get :facebook
      end
      it "redirect to new_user_session_path" do
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:error]).not_to be_nil
      end
    end
  end

  describe :facebook do
    context :with_new_service do
      subject(:new_facebook_service){
        {
          'provider' => 'facebook',
          'extra' => {
            'user_hash' => {
              'email' => 'test@facebook.com',
              'name' => 'test_user',
              'id' => 213213
            }
          }
        }
      }
      before do
        signin(user1)
        request.env['omniauth.auth'] = new_facebook_service
        get :facebook
      end
      it "redirect to root_url" do
        expect(response).to redirect_to(root_url)
        expect(flash[:notice]).to be_nil
      end
    end
    context :with_registered_signin_user_service do
      subject(:registered_facebook_service){
        {
          'provider' => 'facebook',
          'extra' => {
            'user_hash' => {
              'email' => 'user1@facebook.com',
              'name' => 'test_user',
              'id' => 'user1_facebook_uid'
            }
          }
        }
      }
      before do
        signin(user1)
        request.env['omniauth.auth'] = registered_facebook_service
        get :facebook
      end
      it "redirect to root_url" do
        expect(response).to redirect_to(root_url)
        expect(flash[:notice]).not_to be_nil
      end
    end
    context :with_registered_not_signin_user_service do
      subject(:registered_facebook_service){
        {
          'provider' => 'facebook',
          'extra' => {
            'user_hash' => {
              'email' => 'user2@facebook.com',
              'name' => 'test_user2',
              'id' => 'user2_facebook_uid'
            }
          }
        }
      }
      before do
        signin(user1)
        request.env['omniauth.auth'] = registered_facebook_service
        get :facebook
      end
      it "redirect to root_url" do
        expect(response).to redirect_to(root_url)
        expect(flash[:notice]).not_to be_nil
      end
    end
  end

  describe :twitter do
    pending "TODO add specs"
  end

  describe :gplus do
    pending "TODO add specs"
  end
end
