require 'spec_helper'

describe ServicesController do
  include LoginHelper
  fixtures :users, :services

  subject(:user1){ User.where(email: 'test1@gmail.com').first() }

  describe :index do
    context :without_signin do
      before do
        signout()
        get :index
      end
      it "redirect to signin url" do
        expect(response.redirect?).to be_true
        expect(response).to redirect_to(new_user_session_path)
      end
    end
    context :with_signin do
      before do
        signin(user1)
        get :index
      end
      it "show index" do
        expect(response.success?).to be_true
        expect(response).to render_template('index')
        expect(assigns[:services]).to eq(user1.services)
      end
    end
  end
end
