class CreateUriItems < ActiveRecord::Migration
  def change
    create_table :uri_items do |t|
      t.string :url, :limit => 2100
      t.string :title
      t.string :favicon_url, :limit => 2100

      t.timestamps
    end

    create_table :uri_items_users do |t|
      t.integer :uri_item_id
      t.integer :user_id
    end

    add_index :uri_items_users, :uri_item_id
    add_index :uri_items_users, :user_id
  end
end
