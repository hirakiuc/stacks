class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :passwd
      t.datetime :last_logined_at

      t.timestamps
    end
  end
end
