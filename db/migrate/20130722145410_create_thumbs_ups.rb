class CreateThumbsUps < ActiveRecord::Migration
  def change
    create_table :thumbs_ups do |t|
      t.belongs_to(:user)
      t.belongs_to(:uri_item)

      t.timestamps
    end

    add_index :thumbs_ups, :user_id
    add_index :thumbs_ups, :uri_item_id
  end
end
