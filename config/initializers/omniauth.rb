Rails.application.config.middleware.use OmniAuth::Builder do

  provider :twitter,
    Settings.OmniAuth.twitter.consumer_key,
    Settings.OmniAuth.twitter.consumer_secret,
    display: 'popup'

  provider :facebook,
    Settings.OmniAuth.facebook.app_id,
    Settings.OmniAuth.facebook.app_secret,
    display: 'popup'

  provider :gplus,
    Settings.OmniAuth.googleplus.app_key,
    Settings.OmniAuth.googleplus.app_secret,
    scope: Settings.OmniAuth.googleplus.scope
end
