module ApiHelper

  protected
  def valid_url?(url)
    return false if url.nil?
    return false if url.class != String

    begin
      uri = URI.parse(url)
      return ((uri.scheme =~ /^http(s)?$/i) == 0)
    rescue
      false
    end
  end

  def valid_token?(token)
    # TODO validate request token
    (token.instance_of?(String) && (1..30).include?(token.length))
  end

  def respond_unauthorized_error(url, msg)
    h = {
      result: {
        status: 'failure',
        msg: "Unauthorized: #{msg}"
      },
      url: url
    }
    respond_with(h, :status => :unauthorized, :location => nil)
  end

  def respond_parameter_error(url, msg)
    h = {
      result: {
        status: 'failure',
        msg: "Invalid request: #{msg}"
      },
      url: url
    }
    respond_with(h, :status => :bad_request, :location => nil)
  end
end
