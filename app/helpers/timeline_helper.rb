module TimelineHelper

  def thumbs_uped?(uri_item)
    user = current_user()
    return false if (user.nil? or uri_item.nil?)

    ThumbsUp.where(user_id: user.id, uri_item_id: uri_item.id).exists?
  end

  def thumbs_up_id(uri_item)
    user = current_user()
    return nil if (user.nil? or uri_item.nil?)

    m = ThumbsUp.where(user_id: user.id, uri_item_id: uri_item.id).first()
    (m.nil?) ? nil : m.id
  end
end
