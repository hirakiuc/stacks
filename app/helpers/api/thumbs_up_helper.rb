module Api::ThumbsUpHelper
  include ApiHelper

  def validate_request
    url = nil

    if params[:url]
      url = params[:url]
      unless valid_url?(url)
        respond_parameter_error(url, 'invalid url') and return
      end
    elsif params[:itemid]
      item_id = params[:itemid]

      unless item_id =~ /^\d*$/
        respond_parameter_error(url, 'target item not found.') and return
      end
    else
      respond_parameter_error(url, 'invalid parameter') and return
    end

    if params[:token]
      token = params[:token]
      unless valid_token?(token)
        respond_unauthorized_error(url, 'request token is invalid.') and return
      end
    else
      unless user_signed_in?
        respond_unauthorized_error(url, 'you are not signed in.') and return
      end
    end
  end
end
