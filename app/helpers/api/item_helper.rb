module Api::ItemHelper
  include ApiHelper

  def validate_item_request
    url = nil

    if (url = params[:url])
      unless valid_url?(url)
        respond_parameter_error(url, 'invalid url') and return
      end
    elsif (item = params[:uri_item])
      unless valid_url?(item[:url])
        respond_parameter_error(url, 'invalid url') and return
      end
    else
      respond_parameter_error(url, 'invalid parameter') and return
    end

    if (token = params[:token])
      unless valid_token?(token)
        respond_unauthorized_error(url, 'request token is invalid.') and return
      end
    else
      unless user_signed_in?
        respond_unauthorized_error(url, 'you are not signed in.') and return
      end
    end
  end
end
