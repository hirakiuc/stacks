module Service::CallbackHelper

  def parse_omniauth_for(service, omniauth)
    return  case service
            when :facebook
              parse_omniauth_for_facebook(omniauth)
            when :github
              parse_omniauth_for_github(omniauth)
            when :twitter
              parse_omniauth_for_twitter(omniauth)
            when :gplus, :google, :yahoo, :myopenid, :openid
              parse_omniauth_for_openid(omniauth)
            else
              logger.warn "unknown service type:#{service}, omniauth:#{omniauth}"
              {}
            end
  end

  private
  def fetch_hash(hash, keys)
    unless hash.kind_of?(Hash)
      return {}
    end

    h = hash
    keys.each do |key|
      if !(h.kind_of?(Hash)) or !(h[key].kind_of?(Hash))
        h = {}
        break
      end

      h = h[key]
    end

    h
  end

  def parse_omniauth_for_facebook(omniauth)
    h = {}

    user_hash = fetch_hash(omniauth, %w|extra user_hash|)

    h[:email] = (user_hash['email']) ? user_hash['email']   : ''
    h[:name]  = (user_hash['name'])  ? user_hash['name']    : ''
    h[:uid]   = (user_hash['id'])    ? user_hash['id'].to_s : ''
    h[:provider] = (omniauth['provider']) ? omniauth['provider'] : ''

    h
  end

  def parse_omniauth_for_github(omniauth)
    h = {}

    user_info = fetch_hash(omniauth, %w|user_info|)

    h[:email] = (user_info['email']) ? user_info['email'] : ''
    h[:name]  = (user_info['name']) ? user_info['name'] : ''

    user_hash = fetch_hash(omniauth, %w|extra user_hash|)
    h[:uid]   = (user_hash['id']) ? user_hash['id'].to_s : ''

    h[:provider] = omniauth['provider'] ? omniauth['provider'] : ''

    h
  end

  def parse_omniauth_for_twitter(omniauth)
    h = {}

    user_info = fetch_hash(omniauth, %w|info|)

    h[:email] = (user_info['email']) ? user_info['email'] : ''
    h[:name] = (user_info['name']) ? user_info['name'] : ''

    h[:uid] = (omniauth['uid']) ? omniauth['uid'].to_s : ''
    h[:provider] = (omniauth['provider']) ? omniauth['provider'] : ''

    h
  end

  def parse_omniauth_for_openid(omniauth)
    h = {}

    user_info = fetch_hash(omniauth, %w|user_info|)

    h[:email] = (user_info['email']) ? user_info['email'] : ''
    h[:name] = (user_info['name']) ? user_info['name'] : ''
    h[:uid] = (omniauth['uid']) ? omniauth['uid'].to_s : ''

    h[:provider] = omniauth['provider'] ? omniauth['provider'] : ''

    h
  end
end
