class UriItem < ActiveRecord::Base
  attr_accessible :favicon_url, :title, :url

  validates_presence_of :url
  validates_uniqueness_of :url

  has_and_belongs_to_many :users, :order => "last_sign_in_at DESC", :limit => 30
  has_many :comments, :dependent => :destroy
end
