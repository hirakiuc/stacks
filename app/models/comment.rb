class Comment < ActiveRecord::Base
  attr_accessible :text

  validates_presence_of :text

  has_ancestry
  belongs_to :uri_item
  belongs_to :user

  scope :of_user, lambda{|user| joins(:user).where('users.email' => user.email) }
end
