class Service < ActiveRecord::Base
  belongs_to :user

  attr_accessible :provider, :email, :uid, :name, :user_id

  validates_presence_of :provider
  validates_presence_of :name

  validates_presence_of :uid
  validates_uniqueness_of :uid, scope: :provider

  def self.create_with_authhash(authhash)
    Service.create(
      provider: authhash[:provider],
      uid: authhash[:uid],
      name: authhash[:name],
      email: authhash[:email]
    )
  end
end
