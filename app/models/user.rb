class User < ActiveRecord::Base
  has_many :services, :order => "provider"
  has_and_belongs_to_many :uri_items, :order => "created_at DESC", :limit => 30
  has_many :comments, :order => "created_at DESC", :limit => 50

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook, :twitter, :gplus]

  attr_accessible :email, :name, :password, :password_confirmation, :remember_me

  validates_presence_of :name,
    :message => I18n.t('activerecord.errors.messages.blank')
  validates_length_of :name, :within => 4..30,
    :too_long  => I18n.t('activerecord.errors.messages.too_long.other' , 30),
    :too_short => I18n.t('activerecord.errors.messages.too_short.other',  4)
  validates_uniqueness_of :name
  validates_uniqueness_of :email

end
