class ThumbsUp < ActiveRecord::Base
  belongs_to :user
  belongs_to :uri_item

  validates_uniqueness_of :uri_item_id, :scope => :user_id

  scope :for_url, ->(url) {
    joins(:uri_item).where("uri_items.url = ?", url)
  }

  scope :for_user, ->(user) {
    where(user_id: user.id)
  }

  def self.create_with_user_and_url(user, url)
    thumbsUp = ThumbsUp.new()
    thumbsUp.user = user

    item = UriItem.where(url: url).first()
    unless item
      item = UriItem.create(url: url)
    end

    thumbsUp.uri_item = item

    unless thumbsUp.save()
      logger.warn "save failed: #{thumbsUp.errors.full_messages}"
      # TODO invoke job if url_item doesn't have titile, favicon_url
      # TODO invoke job if site_meta isn't exists.
    end

    thumbsUp
  end
end
