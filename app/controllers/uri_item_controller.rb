class UriItemController < ApplicationController
  def show
    logger.debug "item_id:#{params[:id]}"
    begin
      @item = UriItem.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @item = nil
    end
  end
end
