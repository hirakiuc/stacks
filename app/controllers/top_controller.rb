class TopController < ApplicationController
  def index
    if user_signed_in?
      @items = current_user.uri_items()
    else
      @items = UriItem.order("created_at").limit(20).all()
    end
  end
end
