class ServicesController < ApplicationController
  before_filter :authenticate_user!

  protect_from_forgery # :except => :create # see https://github.com/intridea/omniauth/issues/203

  # GET /services
  def index
    @services = current_user.services().to_a
  end

  # DELETE /services/destroy
  def destroy
    @service = current_user.services.find(params[:id])

    if session[:service_id] == @service.id
      flash[:error] = 'You are currently signed in with this account !'
    else
      @service.destroy
    end

    redirect_to services_path
  end
end
