class Api::ItemController < ApplicationController
  include Api::ItemHelper
  respond_to :json

  before_filter :validate_item_request, :only => :create

  def create
    user = current_user()

    status = :ok
    msg = nil

    url = if params[:url]
            params[:url]
          elsif params[:uri_item]
            params[:uri_item][:url]
          else
            nil
          end

    if url
      item = UriItem.where(url: url).first
      if item.nil?
        item = UriItem.create({ url: url })
        status = :created unless item.nil?
      end

      unless item.users.exists?(id: user.id)
        item.users << user
        status = :created
      end

      unless item.save()
        logger.warn "save failed:#{item.errors.full_messages}"
        status = :internal_server_error
        msg = "save failed."
      end
    else
      status = :bad_request
      msg = "invalid parameter"
    end

    respond_with_result({
      :url => url
    }, status, msg)
  end

  private
  def respond_with_result(values, status, msg = nil)
    h = {
      result: {
        status: ([:ok, :created].include?(:ok)) ? 'success' : 'failure',
        msg: msg
      }
    }.merge(values)

    respond_with(h, :status => status, :location => nil)
  end

  def respond_with_error(status, msg = nil)
    h = {
      result: {
        status: 'failure',
        msg: msg
      }
    }

    respond_with(h, :status => status, :location => nil)
  end
end
