class Api::ThumbsUpController < ApplicationController
  include Api::ThumbsUpHelper
  respond_to :json

  before_filter :validate_request, :only => :create

  def create
    user = current_user()
    url = nil

    status = :ok
    msg = nil
    if (url = params[:url])
      thumbsUp = ThumbsUp.for_url(url).for_user(user).first
      unless thumbsUp
        thumbsUp = ThumbsUp.create_with_user_and_url(user, url)
      end

      status = (thumbsUp.persisted?() and thumbsUp.errors.empty? ) ? :ok : :internal_server_error
      msg = thumbsUp.errors.empty? ? nil : thumbsUp.errors.full_messages
    else
      status = :bad_request
      msg = 'invalid request parameter'
    end

    count = ThumbsUp.for_url(url).count()

    respond_with_result({
      :url => url,
      :thumbsupid => thumbsUp.id,
      :count => count
    }, status, msg)
  end


  def destroy
    head :unauthorized and return unless user_signed_in?

    user = current_user()

    status = :not_found
    if (thumbsup_id = params[:id])
      rows = ThumbsUp.delete(thumbsup_id)

      status = (rows >= 1) ? :ok : :not_found
    else
      status = :bad_request
      msg = 'invalid request parameter'
    end

    head status
  end


  private
  def respond_with_result(values, status, msg = nil)
    h = {
      result: {
        status: (status == :ok) ? 'success' : 'failure',
        msg: msg
      }
    }.merge(values)

    respond_with(h, :status => status, :location => nil)
  end

  def respond_with_error(url, status, msg = nil)
    h = {
      result: {
        status: 'failure',
        msg: msg
      }
    }
    h[:url] = url unless url.nil?

    respond_with(h, :status => status, :location => nil)
  end
end
