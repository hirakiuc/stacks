class Service::CallbackController < ApplicationController
  include Service::CallbackHelper

  before_filter :omniauth_filter,    :only => [:facebook, :twitter, :gplus]

  def facebook
    process_callback_for(:facebook)
  end

  def twitter
    process_callback_for(:twitter)
  end

  def gplus
    process_callback_for(:gplus)
  end

  def failure
    logger.debug "failure"
  end

  private
  def omniauth_filter
    unless user_signed_in?
      flash[:alert]  = "you need to sing in with your account on this service."
      redirect_to root_url
      return
    end

    service_route = (params[:action].nil?) ? 'No service recognized (invalid callback)' : params[:action]

    @omniauth = request.env['omniauth.auth']
    logger.debug "omniauth        :#{@omniauth}"
    logger.debug "params[:action]:#{params[:action]}"

    if @omniauth.nil? or params[:action].nil?
      msg  = 'Error while authenticating via '
      msg += service_route.capitalize
      msg += '. The service did not return valid data.'

      flash[:error] = msg
      redirect_to new_user_session_path
      return
    end

    logger.debug "action: #{params[:action]}"
    if service_route != params[:action]
      render :text => @omniauth.to_yaml()
      return
    end

    @authhash = parse_omniauth_for(params[:action].to_sym(), @omniauth)
    logger.debug "authhash: #{@authhash}"

    if (@authhash[:uid].empty? or @authhash[:provider].empty?)
      msg  = 'Error while authenticating via '
      msg += service_route + '/' + @authhash[:provider].capitalize
      msg += '. The service returned invalid data for the user id.'

      flash[:error] = msg
      redirect_to new_user_session_path
      return
    end
  end

  def process_callback_for(provider)
    service = Service.find_by_provider_and_uid(@authhash[:provider], @authhash[:uid]) ||
      Service.create_with_authhash(@authhash)

    if service.user
      if service.user.id == current_user().id
        # already signed in with same user.
        flash[:notice] = "You already signed in with your account."
        redirect_to root_url
      else
        flash[:notice] = "Welcome back, #{service.user.name}."
        # this will throw if service.user is not activated.
        #sign_in_and_redirect service.user, :event => :authentication
        sign_in(:user, service.user, :bypass => true)
        redirect_to root_url
      end
    else
      user = current_user()
      user.services << service

      if user.save()
        # this will throw if service.user is not activated.
        #sign_in_and_redirect user, :event => :authentication
        sign_in(:user, user, :bypass => true)

        # TODO fix redirect path to user's services edit path
        redirect_to root_url
      else
        logger.debug "save failed:#{user.errors.full_messages}"
        flash[:alert] = "Sorry, save failed."
        redirect_to new_user_registration_path
      end
    end
  end
end
