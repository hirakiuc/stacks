# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  $("button.thumbsUp").click ->
    target = $(@)
    method = 'POST'
    path = '/api/thumbs_up'
    if $(@).hasClass("active") and $(@).data("thumbsupid")
      method = 'DELETE'
      path  += '/' + $(@).data("thumbsupid")

    options =
      url: path
      type: method
      data:
        url: $(@).data("uri")
      dataType: 'json'

    req = $.ajax(options)
      .success (data) ->
        if data['thumbsupid']
          target.data("thumbsupid", data['thumbsupid'])
        else
          target.removeAttr("thumbsupid")
      .error (jqxhr, status, error) ->
        $('body').append "ajax error: #{status} - #{error}"
