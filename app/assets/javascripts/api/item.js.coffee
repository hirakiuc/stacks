# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  $("#new_uri_item").submit (event) ->
    # stop submit by html
    event.preventDefault()

    $form = $(@)
    $btn = $form.find("button")

    option =
      url: $form.attr('action')
      type: $form.attr('method')
      data: $form.serialize()
      timeout: 10000 # [sec]
      beforeSend: ->
        $btn.attr('disabled', true)
      complete: ->
        $btn.attr('disabled', false)

    req = $.ajax(option)
      .success (data) ->
        $form[0].reset()
        $('body').append "OK"
      .error (jqxhr, status, error) ->
        $('body').append "ajax error: #{status} - #{error}"

